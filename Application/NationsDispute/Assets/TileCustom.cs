﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
# if UNITY_EDITOR
using UnityEditor;
# endif

using UnityEngine.Tilemaps;

public class TileCustom : Tile {

    public override void RefreshTile(Vector3Int location, ITilemap tilemap)
    {
        for (int yd = -1; yd <= 1; yd++)
        {
            for (int xd = -1; xd <= 1; xd++)
            {
                Vector3Int position = new Vector3Int(location.x + xd, location.y + yd, location.z);
                tilemap.RefreshTile(position);
            }
        }

        TileData tileData = new TileData();
        //GetTileData(location, tilemap, ref tileData);
        //Tilemap tilemap;
        // world
        //tilemap.WorldToCell
        GridInformation test;
        test = new GridInformation();
        test.SetPositionProperty(new Vector3Int(), "test", this);



            
        var mt = tileData.transform;

        Vector3 pos = new Vector3(0, 0, location.y * (-0.01f));
        mt.SetTRS(pos, Quaternion.identity, Vector3.one);

        Debug.LogFormat("test tileData.transform\n{0} \n=======\n{1}", tileData.transform, mt);
        tileData.transform = mt;

#if false
        if (tileData.gameObject != null)
        {
            Vector3 pos = tileData.gameObject.transform.position;
            pos.z = location.y * (-0.01f);
            tileData.gameObject.transform.position = pos;

            Debug.LogFormat("test TileData trns = {0}", tileData.gameObject.transform.position);
        }
#endif
    }

#if UNITY_EDITOR
    // 以下はメニュー項目を加えて RoadTile アセットを作るヘルパーです
    [MenuItem("Assets/Create/TileCustom")]
    public static void CreateTileCustom()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Custom Tile", "New Custom Tile", "Asset", "Save Custom Tile", "Assets");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<TileCustom>(), path);
    }
#endif
}


