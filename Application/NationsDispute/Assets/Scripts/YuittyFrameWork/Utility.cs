﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq.Expressions;

namespace YFW
{
    /// <summary>
    /// ユーティリティー
    /// </summary>
    public class Utility
    {
        
        public static string GetPropertyName<T>(Expression<Func<T>> e )
        {
            var member = (MemberExpression)e.Body;
            return member.Member.Name;
        }

        
    }
}
