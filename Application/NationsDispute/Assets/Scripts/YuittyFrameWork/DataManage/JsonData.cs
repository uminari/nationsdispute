﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

namespace YFW
{
    /// <summary>
    /// Jsonデータ管理
    /// </summary>
    public class JsonData : MonoBehaviour
    {
        /// <summary>
        /// Jsonデータを適用する
        /// </summary>
        /// <param name="json">クラスに適用するJson形式String</param>
        public virtual void LoadJson( string json)
        {
            JsonUtility.FromJsonOverwrite(json, this);
        }

        /// <summary>
        /// Jsonデータをセーブする
        /// </summary>
        /// <returns>Json形式String</returns>
        public virtual string SaveToJson()
        {
            return JsonUtility.ToJson(this);

        }
        
        /// <summary>
        /// 引数のオブジェクトをシリアライズ
        /// </summary>
        /// <typeparam name="T">任意の型</typeparam>
        /// <param name="obj">任意の型のオブジェクト</param>
        /// <returns>シリアライズされたデータ</returns>
        private static string Serialize<T>(T obj)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();
            binaryFormatter.Serialize(memoryStream, obj);
            return Convert.ToBase64String(memoryStream.GetBuffer());
        }
        
        /// <summary>
        /// 引数のテキストをデシリアライズ
        /// </summary>
        /// <typeparam name="T">任意の型</typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        private static T Deserialize<T>(string str)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(str));
            return (T)binaryFormatter.Deserialize(memoryStream);
        }
    }
}
