﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

namespace YFW
{
    /// <summary>
    /// 生データ管理（テキストアセット）
    /// </summary>
    public class RawData : MonoBehaviour
    {
        string      mRawDataPath;
        string      mResourcesPath;

        TextAsset   mTextAsset = null;
        bool        mbLoaded = false;

        /// <summary>
        /// テキストデータとして取得
        /// </summary>
        /// <returns></returns>
        public string GetText()
        {
            if (mbLoaded == false) return string.Empty;
            if (mTextAsset == null) return string.Empty;

            return mTextAsset.text;
        }

        /// <summary>
        /// バイナリデータとして取得
        /// </summary>
        /// <returns></returns>
        public byte[] GetBinary()
        {
            if (mbLoaded == false) return null;
            if (mTextAsset == null) return null;
            return mTextAsset.bytes;
        }

        /// <summary>
        /// スプライトデータとして取得
        /// </summary>
        /// <returns></returns>
        public Sprite GetSprite()
        {
            if (mbLoaded == false) return null;
            if (mTextAsset == null) return null;

            Texture2D tex = new Texture2D(1, 1);
            if (tex.LoadImage(mTextAsset.bytes))
            {
                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            }
            return null;
        }
        
        /// <summary>
        /// 開発時生データの読み込み
        /// </summary>
        /// <param name="rawPath"></param>
        /// <returns></returns>
        public bool loadRawData(string rawPath)
        {
            if( File.Exists(rawPath))
            {

            }
            return false;            
        }

        /// <summary>
        /// 
        /// </summary>
        public void saveRawData()
        {
        }

        public bool loadResource()
        {
            return false;
        }
        public void saveResource()
        {
        }
    }
}
