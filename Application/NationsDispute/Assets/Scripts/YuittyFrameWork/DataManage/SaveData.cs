﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;

namespace YFW
{
    /// <summary>
    /// セーブデータ管理
    /// </summary>
    public class SaveData<T> : JsonData where T : SaveData<T>
    {
        readonly static string filePath = "Assets/SaveData";
        
        /// <summary>
        /// 保持するJsonString
        /// </summary>
        private string jsonString = "";

        public void ReLoad()
        {
            LoadJson(GetJson());
        }

        /// <summary>
        /// データロード
        /// </summary>
        public void Load()
        {
            //_instance = JsonUtility.FromJson<SaveData<T>>(GetJson());
            LoadJson(GetJson());
        }

        /// <summary>
        /// 保存しているJsonを取得する
        /// </summary>
        /// <returns></returns>
        private string GetJson()
        {
            //既にJsonを取得している場合はそれを返す。
            if (!string.IsNullOrEmpty(jsonString))
            {
                return jsonString;
            }

#if UNITY_WEBGL
            jsonString = DataAccessWebGL.Load(GetSaveDataPath());
            if(string.IsNullOrEmpty(jsonString))
            {
                jsonString = JsonUtility.ToJson(new SaveData<T>());
            }
#else
            //Jsonを保存している場所のパスを取得。
            string filePath = GetSaveDataPath();

            //Jsonが存在するか調べてから取得し変換する。存在しなければ新たなクラスを作成し、それをJsonに変換する。
            if (File.Exists(filePath))
            {
                jsonString = File.ReadAllText(filePath);
            }
            else
            {
                jsonString = JsonUtility.ToJson(new SaveData<T>());
            }
#endif
            return jsonString;
        }

        /// <summary>
        /// データをセーブする
        /// </summary>
        public void Save()
        {
#if UNITY_WEBGL
            DataAccessWebGL.Save(this.SaveToJson(), GetSaveDataPath());
#else
            File.WriteAllText(GetSaveDataPath(), this.SaveToJson());
#endif
        }

        /// <summary>
        /// データ削除
        /// </summary>
        public void Delete()
        {
            jsonString = JsonUtility.ToJson(new SaveData<T>());
            ReLoad();
        }

        /// <summary>
        /// セーブデータ階層
        /// </summary>
        /// <returns></returns>
        private static string GetSaveDataPath()
        {
            string path = string.Format("{0}/{1}", filePath, typeof(T).ToString());
#if UNITY_EDITOR
            path += ".json";
#elif UNITY_WEBGL
            //path = Application.persistentDataPath + "/" + filePath + ".dat";
            path = Application.persistentDataPath + "/SaveData.dat";
            return path;
#else
            path = Application.persistentDataPath + "/" + filePath;
#endif
            System.IO.FileInfo file = new System.IO.FileInfo(path);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            return path;
        }

        /// <summary>
        /// ロードされたときにデータを適用する処理
        /// </summary>
        public virtual void ApplyDataLoaded()
        {
        }
    }
}
