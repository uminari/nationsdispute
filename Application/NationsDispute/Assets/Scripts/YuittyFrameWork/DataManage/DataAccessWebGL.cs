﻿#if UNITY_WEBGL

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace YFW
{
    public class DataAccessWebGL
    {
        [DllImport("__Internal")]
        private static extern void SyncFiles();

        [DllImport("__Internal")]
        private static extern void WindowAlert(string message);

        /// <summary>
        /// データセーブアクセス処理
        /// </summary>
        /// <param name="saveJson">保存したいJsonデータ</param>
        /// <param name="dataPath">保存したいパス</param>
        public static void Save(string saveJson, string dataPath)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream;

            try
            {
                if (File.Exists(dataPath))
                {
                    File.WriteAllText(dataPath, string.Empty);
                    fileStream = File.Open(dataPath, FileMode.Open);
                }
                else
                {
                    fileStream = File.Create(dataPath);
                }

                binaryFormatter.Serialize(fileStream, saveJson);
                fileStream.Close();

                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    SyncFiles();
                }
            }
            catch (Exception e)
            {
                PlatformSafeMessage("Failed to Save: " + e.Message);
            }
        }

        /// <summary>
        /// データロードアクセス処理
        /// </summary>
        /// <param name="dataPath">呼び出したいパス</param>
        /// <returns>呼び出されたJsonString</returns>
        public static string Load(string dataPath)
        {
            string loadData = null;

            try
            {
                if (File.Exists(dataPath))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    FileStream fileStream = File.Open(dataPath, FileMode.Open);

                    loadData = (string)binaryFormatter.Deserialize(fileStream);
                    fileStream.Close();
                }
            }
            catch (Exception e)
            {
                PlatformSafeMessage("Failed to Load: " + e.Message);
            }

            return loadData;
        }

        private static void PlatformSafeMessage(string message)
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                WindowAlert(message);
            }
            else
            {
                Debug.Log(message);
            }
        }
    }

}
#endif // UNITY_WEBGL