﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YFW
{
    /// <summary>
    /// スクリーンショットマネージャー（シングルトンシステム）
    /// </summary>
    public class ScreenShotManager : SystemManager<ScreenShotManager>
    {
        public  string  m_FilePath;         // ファイルパスの指定
        public  bool    m_IsOnCamera;       // カメラ上に設定した
        bool    mbTakeScreenshot = false;   // スクリーンショットトリガー
        int     mCount = 0;

        /// <summary>
        /// 任意のタイミングでスクリーンショットトリガーを行う。（コード中呼び出し可能）
        /// </summary>
        public void takeScreenShot()
        {
            mbTakeScreenshot = true;
            if (!m_IsOnCamera)
            {
                StartCoroutine(delayScreenShot());
            }
        }

        /// <summary>
        /// キー入力認識
        /// </summary>
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Backslash))
            {
                takeScreenShot();
            }
        }
        
        /// <summary>
        /// コルーチンで遅延して適用
        /// </summary>
        /// <returns></returns>
        IEnumerator delayScreenShot()
        {
            yield return new WaitForEndOfFrame();

            CreateScreenShotTexture();
        }

        /// <summary>
        /// レンダーポスト後に適用
        /// </summary>
        private void OnPostRender()
        {
            if( ( mbTakeScreenshot == true) && (m_IsOnCamera) )
            {
                CreateScreenShotTexture();
            }
        }

        /// <summary>
        /// スクリーンショット作成
        /// </summary>
        private void CreateScreenShotTexture()
        {
            var texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            texture.Apply();

            var bytes = texture.EncodeToPNG();
            string path = string.Format("{0}/capture_{1:00}.png", m_FilePath, mCount);
            System.IO.File.WriteAllBytes(path, bytes);

            Destroy(texture);
            mbTakeScreenshot = false;
            mCount++;

            Debug.LogFormat("Take ScreenShot Success !! = {0}", path);
        }
    }
}
