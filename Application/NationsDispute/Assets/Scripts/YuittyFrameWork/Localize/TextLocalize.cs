﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace YFW
{
    /// <summary>
    /// テキストローカライズコンポーネント
    /// </summary>
    [Serializable]
    public class TextLocalize : MonoBehaviour
    {
        public string   m_Category;
        public string   m_Key;
        private Text    m_Text;

        private void Awake()
        {
            m_Text = GetComponent<Text>();
        } 

        void Start()
        {
            if(m_Text && LocalizeManager.Instance)
            {
                m_Text.text = LocalizeManager.Instance.getContents(m_Category, m_Key);
            }
        }
    }
}
