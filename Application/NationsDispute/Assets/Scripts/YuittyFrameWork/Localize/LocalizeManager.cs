﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace YFW
{
    /// <summary>
    /// ローカライズ管理
    /// </summary>
    public class LocalizeManager : SystemManager<LocalizeManager>
    {
        // Dictionary< key(カテゴリ),Value(Dictionary< key(メンバ名),Value(値) >) >
        private Dictionary<string, Dictionary<string, string>> mLocalizeDataList = new Dictionary<string, Dictionary<string, string>>();

        [SerializeField]
        public LocalizeData mActiveLocalizeData = null;
#if UNITY_EDITOR
        public SystemLanguage nowloadedLocalizeCode;
        public SystemLanguage nextloadedLocalizeCode;

        public bool bDebugLocalize = false;
        public SystemLanguage debugloadedLocalizeCode;
#endif

        readonly static string imageFilePath = "TutorialImage";
        public Sprite          tutorialSpr = null;

        public void Start()
        {
            setupLocalizeData();
        }

        /// <summary>
        /// ローカライズデータのセットアップ
        /// </summary>
        public void setupLocalizeData()
        {
            // 起動時に新しいLocalizeDataを取得してセット
            mActiveLocalizeData = gameObject.AddComponent<LocalizeData>();

            SystemLanguage lang = SystemLanguage.English;
            // 言語未対応がある場合はすべて英語化
            switch( Application.systemLanguage )
            {
                case SystemLanguage.Japanese:
                    lang = SystemLanguage.Japanese;
                    break;
                default:
                    lang = SystemLanguage.English;
                    break;
            }
#if UNITY_EDITOR
            if( bDebugLocalize)
            {
                lang = debugloadedLocalizeCode;
            }
#endif
            // 言語情報をセットアップ
            if (loadLocalizeData(lang))
            {
                mActiveLocalizeData.setLocalizeDataList(ref mLocalizeDataList);
            }
            // チュートリアルテクスチャをセットアップ
            loadLocalizeImage(lang);
        }

        /// <summary>
        /// 指定したローカライズIDのデータを呼び出す。
        /// </summary>
        /// <param name="localizeid"></param>
		public bool loadLocalizeData(SystemLanguage localizelang)
        {			
            if( mActiveLocalizeData == null)
            {
                mActiveLocalizeData = gameObject.AddComponent<LocalizeData>();
            }
            mActiveLocalizeData.Load(localizelang);

			return true;
        }

        /// <summary>
        /// 指定したカテゴリの文字列データを取得
        /// </summary>
        /// <param name="category"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string getContents(string category, string key)
        {
            if( mLocalizeDataList.ContainsKey(category))
            {
                if( mLocalizeDataList[category].ContainsKey(key))
                {
                    return mLocalizeDataList[category][key];
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localizelang"></param>
        private void loadLocalizeImage(SystemLanguage localizelang)
        {
            string path = string.Format("{0}/tutorial_{1}", imageFilePath, localizelang.ToString());
            ResourceLoader.Instance.loadAsync(path, (handle) => {
                var tex = handle as Texture2D;
                tutorialSpr = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            });
        }

    }
#if UNITY_EDITOR
    [CustomEditor(typeof(LocalizeManager))]
    public class LoacalizeManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {

            LocalizeManager manager = target as LocalizeManager;

            EditorGUILayout.LabelField("読み込み中のローカライズ", manager.nowloadedLocalizeCode.ToString());
            manager.nextloadedLocalizeCode = (SystemLanguage)EditorGUILayout.EnumPopup("ローカライズコード指定", manager.nextloadedLocalizeCode);

            if (GUILayout.Button("指定ローカライズ呼び出し"))
            {
                if (manager.loadLocalizeData(manager.nextloadedLocalizeCode))
                {
                    manager.nowloadedLocalizeCode = manager.nextloadedLocalizeCode;
                }
            }

            manager.bDebugLocalize = EditorGUILayout.Toggle("ローカライズデバッグ", manager.bDebugLocalize);
            manager.debugloadedLocalizeCode = (SystemLanguage)EditorGUILayout.EnumPopup("デバッグ読み込みローカライズ対象", manager.debugloadedLocalizeCode);
        }
    }
#endif
}
