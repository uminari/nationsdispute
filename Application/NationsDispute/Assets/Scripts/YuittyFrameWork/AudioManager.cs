﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace YFW
{
    /// <summary>
    /// オーディオ管理
    /// </summary>
    public class AudioManager : SystemManager<AudioManager>
    {
        
        // マスターRootミキサー(全てのMixierの一番親)
        public AudioMixer mMasterMixer;

        public string mBGMVolumeParameter;  // BGMボリューム管理パラメータキー
        public string mSEVolumeParameter;   // SEボリューム管理パラメーターキー

        private readonly float MinDbValue = -80f;   // 最小デシベル

        private float _bgmVolume = 1f;
        private float _seVolume = 1f;

        public GameObject mAudioSourcePrefab;
        public Dictionary<string,AudioSource> mAudioSourceDict = new Dictionary<string,AudioSource>();

        /// <summary>
        /// 0～1floatをDb Volume値に変更
        /// </summary>
        /// <param name="value">0～1のSlider Value</param>
        /// <returns>Db Valueで返却</returns>
        private float changeDb(float value)
        {
            return (1f - value) * MinDbValue;
        }

        /// <summary>
        /// BGM Volume変更
        /// </summary>
        public float BGMVolume {
            set {
                if (_bgmVolume != value)
                {
                    if (mMasterMixer.SetFloat(mBGMVolumeParameter, changeDb(value)))
                    {
                        _bgmVolume = value;
                    }
                }
            }
            get { return _bgmVolume; }
        }

        /// <summary>
        /// SE Volume変更
        /// </summary>
        public float SEVolume {
            set
            {
                if (_seVolume != value)
                {
                    if (mMasterMixer.SetFloat(mSEVolumeParameter, changeDb(value)))
                    {
                        _seVolume = value;
                    }
                }
            }
            get { return _seVolume; }
        }

        public void Start()
        {
            setupSourceList();
        }

        /// <summary>
        /// AudioSourceListのリンクを張る
        /// </summary>
        private void setupSourceList()
        {
            if( mAudioSourcePrefab != null)
            {
                var sourcelist = mAudioSourcePrefab.GetComponentsInChildren<AudioSource>();
                foreach ( var source in sourcelist)
                {
                    // クリップ名でキーを張る.
                    mAudioSourceDict.Add(source.clip.name, source);
                }
            }
        }

        /// <summary>
        /// サウンド再生
        /// </summary>
        /// <param name="key"></param>
        public void PlaySe(string key)
        {
            if (mAudioSourceDict.ContainsKey(key))
            {
                mAudioSourceDict[key].Play();
            }
        }


        private string _playBgmKey = "";
        public void PlayBGM(string key)
        {
            if (mAudioSourceDict.ContainsKey(key))
            {
                mAudioSourceDict[key].Play();
                _playBgmKey = key;
            }
        }

        public void StopBGM()
        {
            if(!string.IsNullOrEmpty(_playBgmKey))
            {
                mAudioSourceDict[_playBgmKey].Stop();
                _playBgmKey = string.Empty;
            }
        }
    }
}
