﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 国民クラス
/// </summary>
public class Citizen : MonoBehaviour {

    public AppDefine.eCitizenType mType;    // 国民タイプ

    /// <summary>
    /// 帰属先プレイヤー
    /// </summary>
    public Player mAttrPlayer;
    /// <summary>
    /// チーム帰属ID(プレイヤーのものを参照)
    /// </summary>
    public int TeamId { get { return mAttrPlayer.mPlayerID; } }

    /// <summary>
    /// 現在のカレントポイント
    /// </summary>
    public Vector3Int mCurrentPoint;

    /// <summary>
    /// リソース出力
    /// </summary>
    public GameResourceData mOutputResource;
    /// <summary>
    /// 維持費
    /// </summary>
    public GameResourceData mInputResource;

    /// <summary>
    /// 戦闘ユニットの攻撃力
    /// </summary>
    public int Power { get { return mOutputResource.Power; } }

    /// <summary>
    /// 難民カウンタ
    /// </summary>
    public int mRefugeeCount;

    /// <summary>
    /// フィールド上に市民を置く
    /// </summary>
    public void PutFieldCitizen(Vector3Int point)
    {
        setCitizen(point);
    }

    /// <summary>
    /// 市民を指定位置に移動させる
    /// </summary>
    /// <param name="movepoint"></param>
    public void MoveCitizen(Vector3Int movepoint)
    {

        /*
         * 
         * 移動可能な位置(Y座標が2で割り切れる数かどうかで適切な位置を判断するパターンを作る)
         * 
         *     (0,+1) (+1,+1)
         * 　(-1, 0) * (+1, 0)  
         *     (0,-1) (+1,-1)
         *     
         *     (-1,+1) ( 0,+1)
         *     
         *     
         */
        setCitizen(movepoint);
    }

    /// <summary>
    /// 市民の配置処理
    /// </summary>
    private void setCitizen(Vector3Int point)
    {

    }

    /// <summary>
    /// 帰属先の変更
    /// </summary>
    /// <param name="newPlayer"></param>
    /// <returns></returns>
    public bool changeAttibution(Player newPlayer)
    {
        return newPlayer.AddCitizen(this);
    }
    
    /// <summary>
    /// 市民の喪失(兵士・射手のみ呼び出されるのでチェック)
    /// </summary>
    public bool DeadCitizen()
    {
        if(( mType == AppDefine.eCitizenType.Solder)||(mType == AppDefine.eCitizenType.Archer))
        {
            mAttrPlayer.RemoveCitizen(this);
            return true;
        }
        return false;
    }

    /// <summary>
    /// 難民に変化させる
    /// </summary>
    public void changeRefugee()
    {

    }

    /// <summary>
    /// 戦闘の実行
    /// </summary>
    public void Battle(Vector3Int battlepoint)
    {

    }

    /// <summary>
    /// 領地に組み込む
    /// </summary>
    public void setAttributeFlag()
    {
        // 自分の座標からタイル情報(TileInfo)を読み込んで、DemarcationOfLandを呼び出す
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
