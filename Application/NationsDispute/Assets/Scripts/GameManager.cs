﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ゲーム管理システム
/// </summary>
public class GameManager : YFW.SystemManager<GameManager> {

    /*
     * ゲーム全体の管理をするゲームマスタークラス
     * 
     * 各プレイヤーへの進行順番指示と、ゲームセットのチェックを行う
     * 
     * またゲーム全体で一括管理されるアイテムの状態を管理もする
     * 
     * 
     */

    public void GameStep()
    {

    }

    /// <summary>
    /// ゲームセットの状態をチェックする
    /// </summary>
    /// <returns></returns>
    public bool checkGameSet()
    {
        return false;
    }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
