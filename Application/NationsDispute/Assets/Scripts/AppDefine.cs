﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AppDefine
{
    /// <summary>
    /// 色ID定義
    /// </summary>
    public enum eColorId
    {
        INVALID = -1,
        RED,
        BLUE,
        YELLOW,
        GREEN,
        SKY,

        MAX = 5
    };

    public static int COLOR_NUM_MAX = 4;
    

    /// <summary>
    /// 色値対応表
    /// </summary>
    public static Dictionary<eColorId, Color> ColorDict = new Dictionary<eColorId, Color>()
    {
        { eColorId.RED,    new Color(1.0f, 0.1569f, 0f) },
        { eColorId.BLUE,   new Color(0f, 0.2549f, 1f) },
        { eColorId.YELLOW, new Color(1f, 0.9608f, 0f) },
        { eColorId.GREEN,  new Color(0.2078f,0.6314f,0.4196f) },
        { eColorId.SKY,    new Color(0.4f,0.8f,1f) },
    };
    

    public static readonly string TextParamFormat = "{0:0000}";


    public static string GROUND_TAG = "Ground";
    public static string STAGEOUT_TAG = "";
    public static string WALL_TAG = "Wall";
    public static string VOID_TAG = "Void";

    public static string PLAYER_TAG = "Player";
    public static string ENEMY_TAG = "Enemy";
    public static string ITEM_TAG = "Item";


    /// <summary>
    /// 自然地形タイル タイプ
    /// </summary>
    public enum eNatureTileType
    {
        INVALID = -1,
        GRASS,
        FOREST,
        MOUNTAIN,
        WATER,
    }

    /// <summary>
    /// 建造物タイル　タイプ
    /// </summary>
    public enum eBuildingTileType
    {
        Invalid = -1,
        None,
        Capital,
        Farm,
        Market,
        GatherersHuts,
        WoodCutter,
        Quarry,
        Mine,
        Fishboat,
        Harbor,
        Fort,
        LightHouse,
        ArmsShop,
        WorkShop,
        Monument,
    }

    /// <summary>
    /// 市民タイル　タイプ
    /// </summary>
    public enum eCitizenType
    {
        Invalid = -1,
        None,
        Settler,        // 開拓民
        Worker,         // 労働者
        Merchant,       // 商人
        Solder,         // 兵士
        Archer,         // 射手
        Philosopher,    // 哲学者
        Refugee,        // 難民
    }

    public static Dictionary<int, Color32> mTeamColor = new Dictionary<int, Color32>()
    {
        { 0,new Color32(  0,255,  0,255) },
        { 1,new Color32(  0,255,  0,255) },
        { 2,new Color32(  0,255,  0,255) },
        { 3,new Color32(  0,255,  0,255) },
        { 4,new Color32(  0,255,  0,255) },
        { 5,new Color32(  0,255,  0,255) },
        { 6,new Color32(  0,255,  0,255) },
        { 7,new Color32(  0,255,  0,255) },
    };





    //---------------------------------
    // 時間のカウンタ定義
    //---------------------------------
    /// <summary>
    /// 単位時間
    /// </summary>
    public static float UNIT_TIME = 0.2f;

    /// <summary>
    /// 60分1時間
    /// </summary>
    public static int MINUTE_TO_HOURS = 60;
    /// <summary>
    /// 24時間1日
    /// </summary>
    public static int HOURS_TO_DAYS = 24;
    /// <summary>
    /// 10日1月 (プレイ感によりここの変更の余地あり)
    /// </summary>
    public static int DAYS_TO_MONTH = 10;
    /// <summary>
    /// 12月1年
    /// </summary>
    public static int MONTH_TO_YEAR = 12;
}
