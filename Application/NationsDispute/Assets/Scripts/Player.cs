﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤークラス
/// </summary>
public class Player : MonoBehaviour {

    /*
     * プレイヤー別の進捗の管理や、プレイヤーのゲーム状態を保持するクラス
     * 
     * 
     * 
     */

    
    /// <summary>
    /// 所属している国民リスト
    /// </summary>
    public List<Citizen> mCitizens;

    /// <summary>
    /// プレイヤーID
    /// </summary>
    public int mPlayerID;

    /// <summary>
    /// 直前のターンのリソース保持量
    /// </summary>
    public GameResource mOldTurnGameResource;
    /// <summary>
    /// プレイヤー個別のゲームリソース保持量
    /// </summary>
    public GameResource mMyGameResrouce;

    /// <summary>
    /// リソースが赤字になっているものが存在する。
    /// </summary>
    public bool mbDeficit;

    
    /// <summary>
    /// 国民を所有する
    /// </summary>
    /// <param name="citizen">対象の国民</param>
    public bool AddCitizen(Citizen citizen)
    {
        // 既に所属している場合
        if (citizen.mAttrPlayer == this)
        {
            Debug.LogWarningFormat("既に該当の国民を所有している！ [ Player:{0} / Citizen:{1} ]", this, citizen);
            return false;
        }
        // 違うプレイヤーに帰属している場合
        if (citizen.mAttrPlayer != this)
        {
            citizen.mAttrPlayer.RemoveCitizen(citizen);
        }
        citizen.mAttrPlayer = this;
        mCitizens.Add(citizen);

        return true;
    }

    /// <summary>
    /// 国民を所有から外す
    /// </summary>
    /// <param name="citizen">対象の国民</param>
    public bool RemoveCitizen(Citizen citizen)
    {
        return mCitizens.Remove(citizen);
    }

    /// <summary>
    /// プレイヤーのターン開始時リソース出力を計算する
    /// </summary>
    public void CalcResourcePower()
    {

    }

    /// <summary>
    /// 維持費の計算
    /// </summary>
    public void CalcMaintenanceCosts()
    {

    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
