﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.IO;
using YFW;

/// <summary>
/// ステージデータ (ステージごとに用意)
/// </summary>
[Serializable]
public class ConfigSaveData : SaveData<ConfigSaveData>
{
    /// <summary>
    /// オプション設定データ
    /// </summary>
    [Serializable]
	public class Prefarence{
		public float _BGMVolume;
        public float _SEVolume;
        public int   _lastSelectShape;
        public int   _lastSelectColor;
        public bool  _lastRandomOn;
        public int   _tutorialStep;
        public Prefarence() { _BGMVolume = 1f; _SEVolume = 1f; _lastSelectShape = 3; _lastSelectColor = 2; _lastRandomOn = false; _tutorialStep = (int)eTutorialStep.NEW; }
    }

    public enum eTutorialStep
    {
        INVALID = -1,
        NEW = 0,    // 未プレイフラグ
        PLAYED,     // プレイ済フラグ
    }

    public enum eStageProgress
    {
        INVALID = -1,
        NEW = 0,    // 未プレイフラグ
        PLAYED,     // プレイ済フラグ
        CLEAR,      // クリアフラグ
    }
    /// <summary>
    /// ステージ情報
    /// </summary>
    [Serializable]
    public class StageProgress
    {
        public int              _stageID;
        public eStageProgress   _progressNum;
        public int              _best_rotateNum;
        public int              _best_changeNum;
        public int              _best_time;
    }

    /// <summary>
    /// Preference
    /// </summary>
    [SerializeField]
    public Prefarence       mPreferenceInfo;
    /// <summary>
    /// Progresses
    /// </summary>
    [SerializeField]
    public StageProgress[]  mProgressInfo;

    /// <summary>
    /// constractor
    /// </summary>
    public ConfigSaveData()
    {
        clearData();
    }

    /// <summary>
    /// データの初期化
    /// </summary>
    public void clearData()
    {
        mPreferenceInfo = new Prefarence();
#if false
        int progressNum = (int)AppDefine.eShapeNum.MAX * (int)AppDefine.eColorId.MAX;
        mProgressInfo = new StageProgress[progressNum];
        int l = 0;
        for (int i = 0; i < (int)AppDefine.eShapeNum.MAX; i++)
        {
            for (int j = 0; j < AppDefine.COLOR_NUM_MAX; j++)
            {
                mProgressInfo[l] = new StageProgress() { _stageID = (int)AppDefine.shapeLink[i] * 10 + j+2, _progressNum = eStageProgress.NEW, _best_time = -1, _best_rotateNum = -1, _best_changeNum = -1 };
                l++;
            }
        }
#endif
    }

    /// <summary>
    /// ステージのプログレス情報を返却
    /// </summary>
    /// <param name="stageId">図形番号x10+色番号</param>
    /// <returns></returns>
    public void getProgressSaveData(ref StageProgress progress, int stageId)
    {
#if false
        progress = null;
        if (mProgressInfo != null)
        {
            for (int i = 0; i < AppDefine.StageID_MAX; i++)
            {
                if (mProgressInfo[i]._stageID == stageId)
                {
                    progress = mProgressInfo[i];
                }
            }
        }
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    public override void ApplyDataLoaded()
    {
        // サウンド情報を適用.
        if( AudioManager.Instance)
        {
            AudioManager.Instance.BGMVolume = mPreferenceInfo._BGMVolume;
            AudioManager.Instance.SEVolume = mPreferenceInfo._SEVolume;
        }
    }
}
