﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using YFW;
using System.IO;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// ローカライズデータ (ローカライズごとに用意)
/// </summary>
[Serializable]
public class LocalizeData : JsonData
{
    readonly static string filePath = "Localize";

    // --- 子階層の生成をクラスで行う. --- 
    public interface IMenu_base
    {
        /// <summary>
        /// カテゴリ内のデータを返却する
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> getData();
    }

    // クラスを追加し、Editorでプロパティをセットする
    [Serializable]
    public class TITLE_strings : IMenu_base
    {
        public string Title_Sub;        // タイトルロゴ下の文字
        public string Start_Button;     // タイトル：はじめるボタン
        public Dictionary<string, string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>Title_Sub),       Title_Sub },
                { Utility.GetPropertyName(()=>Start_Button),    Start_Button },
            };
        }
    }

    [Serializable]
    public class System_strings : IMenu_base
    {
        public string QuitApp;          // アプリケーション終了
        public string Cancel;           // キャンセル
        public string Exit;             // 終了
        public Dictionary<string, string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>QuitApp),       QuitApp },
                { Utility.GetPropertyName(()=>Cancel ),       Cancel  },
                { Utility.GetPropertyName(()=>Exit   ),       Exit    },
            };
        }
    }

    [Serializable]
    public class StageSelect_strings : IMenu_base
    {
        public string SS_Title;     // ステージセレクト：タイトル
        public string SS_Shape;     // ステージセレクト：形
        public string SS_Color;     // ステージセレクト：色
        public string SS_Random;    // ステージセレクト：ランダム
        public string SS_Submit;    // ステージセレクト：決定

        public Dictionary<string, string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>SS_Title),        SS_Title  },
                { Utility.GetPropertyName(()=>SS_Shape),        SS_Shape  },
                { Utility.GetPropertyName(()=>SS_Color),        SS_Color  },
                { Utility.GetPropertyName(()=>SS_Random),       SS_Random },
                { Utility.GetPropertyName(()=>SS_Submit),       SS_Submit },
            };
        }
    }

    [Serializable]
    public class MENU_strings : IMenu_base
    {
        public string Menu_Title;       // メニューのタイトル
        public string Menu_Retry;       // メニューのリトライ
        public string Menu_Retire;      // メニューのリタイア
        public string Menu_Close;       // メニューから離脱（つづける）

        public string Option_Title;     // オプションのタイトル
        public string Option_BGM;       // BGM
        public string Option_SE;        // SE
        public string Option_OK;        // OK

        public string How_To_Play;      // チュートリアルタイトル

        public string Header_Rotate;    // ヘッダー：回転
        public string Header_Change;    // ヘッダー：交換
        public string Header_Time;      // ヘッダー：時間

        public string To_Title;         // タイトルへ
        public string To_Stage_Select;   // ステージ選択へ
        public string To_Replay;        // もう一度？
        public string Yes;              // はい
        public string No;               // いいえ
        public string Retry_Title;      // りとらい？
        public string Retire_Title;     // あきらめる？

        public string Set_Random;       // ランダム化

        public string Tweet;            // ハイスコアの結果をつぶやく
        public string TweetSentence;    // つぶやく文言

        public Dictionary<string, string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>Menu_Title),    Menu_Title },
                { Utility.GetPropertyName(()=>Menu_Retry),    Menu_Retry },
                { Utility.GetPropertyName(()=>Menu_Retire),   Menu_Retire },
                { Utility.GetPropertyName(()=>Menu_Close),    Menu_Close },
                { Utility.GetPropertyName(()=>Option_Title),  Option_Title },
                { Utility.GetPropertyName(()=>Option_BGM),    Option_BGM },
                { Utility.GetPropertyName(()=>Option_SE),     Option_SE },
                { Utility.GetPropertyName(()=>Option_OK),     Option_OK },
                { Utility.GetPropertyName(()=>How_To_Play),   How_To_Play },
                { Utility.GetPropertyName(()=>Header_Rotate), Header_Rotate },
                { Utility.GetPropertyName(()=>Header_Change), Header_Change },
                { Utility.GetPropertyName(()=>Header_Time),   Header_Time },
                { Utility.GetPropertyName(()=>To_Title),      To_Title },
                { Utility.GetPropertyName(()=>To_Stage_Select),To_Stage_Select },
                { Utility.GetPropertyName(()=>To_Replay),     To_Replay },
                { Utility.GetPropertyName(()=>Yes),           Yes },
                { Utility.GetPropertyName(()=>No),            No },
                { Utility.GetPropertyName(()=>Retry_Title),   Retry_Title },
                { Utility.GetPropertyName(()=>Retire_Title),  Retire_Title },
                { Utility.GetPropertyName(()=>Set_Random),    Set_Random },
                { Utility.GetPropertyName(()=>Tweet),         Tweet },
                { Utility.GetPropertyName(()=>TweetSentence), TweetSentence },
                
            };
        }
    }

    [Serializable]
    public class INGAME_strings : IMenu_base
    {
        public string Ready_title;
        public string Ready_OK_label;
        public string Ready_Shape_label;
        public string Ready_Color_label;
        public string Ready_Hiscore_label;

        public string Clear_Congratulations;
        public string Clear_Time;
        public string Clear_Rotate;
        public string Clear_Change;
        public string Clear_Hiscore;

        public Dictionary<string,string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>Ready_title),       Ready_title },
                { Utility.GetPropertyName(()=>Ready_OK_label),    Ready_OK_label },
                { Utility.GetPropertyName(()=>Ready_Shape_label), Ready_Shape_label },
                { Utility.GetPropertyName(()=>Ready_Color_label), Ready_Color_label },
                { Utility.GetPropertyName(()=>Ready_Hiscore_label), Ready_Hiscore_label },                
                { Utility.GetPropertyName(()=>Clear_Congratulations), Clear_Congratulations },
                { Utility.GetPropertyName(()=>Clear_Time),        Clear_Time },
                { Utility.GetPropertyName(()=>Clear_Rotate),      Clear_Rotate },
                { Utility.GetPropertyName(()=>Clear_Change),      Clear_Change },
                { Utility.GetPropertyName(()=>Clear_Hiscore),     Clear_Hiscore },
            };
        }
    }

    [Serializable]
    public class Data_strings : IMenu_base
    {
        public string Shape_03; // 三角形
        public string Shape_04; // 四角形
        public string Shape_06; // 六角形
        public string Shape_08; // 八角形
        public string Shape_10; // 十角形
        public string Shape_12; // 十二角形

        public string Color_2;  // ２色
        public string Color_3;  // ３色
        public string Color_4;  // ４色
        public string Color_5;  // ５色

        public Dictionary<string, string> getData()
        {
            return new Dictionary<string, string>()
            {
                { Utility.GetPropertyName(()=>Shape_03), Shape_03 },
                { Utility.GetPropertyName(()=>Shape_04), Shape_04 },
                { Utility.GetPropertyName(()=>Shape_06), Shape_06 },
                { Utility.GetPropertyName(()=>Shape_08), Shape_08 },
                { Utility.GetPropertyName(()=>Shape_10), Shape_10 },
                { Utility.GetPropertyName(()=>Shape_12), Shape_12 },
                { Utility.GetPropertyName(()=>Color_2), Color_2 },
                { Utility.GetPropertyName(()=>Color_3), Color_3 },
                { Utility.GetPropertyName(()=>Color_4), Color_4 },
                { Utility.GetPropertyName(()=>Color_5), Color_5 },
            };
        }
    }

    // --- 生成したクラスでメンバを作成する . 実際に管理するローカライズデータ
    public SystemLanguage   localizeLanguage;
    public TITLE_strings    Category_Title;
    public System_strings   Category_System;
    public StageSelect_strings Category_StageSelect;
    public MENU_strings     Category_Menu;
    public INGAME_strings   Category_Ingame;
    public Data_strings     Category_Data;

    public LocalizeData() { }
    /// <summary>
    /// 言語設定コンストラクタ
    /// </summary>
    /// <param name="language"></param>
    public LocalizeData(SystemLanguage language)
    {
        localizeLanguage = language;
    }
    
    /// <summary>
    /// ローカライズデータをDict型で返却する
    /// </summary>
    /// <param name="localizeDatalist"></param>
    public void setLocalizeDataList( ref Dictionary<string, Dictionary<string, string>> localizeDatalist )
    {
        localizeDatalist = new Dictionary<string, Dictionary<string, string>>();
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_Title), Category_Title.getData());
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_System), Category_System.getData());
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_StageSelect), Category_StageSelect.getData());
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_Menu), Category_Menu.getData());
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_Ingame), Category_Ingame.getData());
        localizeDatalist.Add(Utility.GetPropertyName(() => Category_Data), Category_Data.getData());
    }

    /// <summary>
    /// 指定したロケーションのデータをロードする
    /// </summary>
    public void Load(SystemLanguage language)
    {
		string filePath = GetSaveDataPath(language);
        string jsonString = string.Empty;
        //Jsonが存在するか調べてから取得し変換する。存在しなければ自身をJsonに変換する。
#if false
        if (File.Exists(filePath))
        {
            jsonString = File.ReadAllText(filePath);
        }
        else
        {
            jsonString = JsonUtility.ToJson( new LocalizeData(language));
        }
#else
        TextAsset _Text = Resources.Load(filePath) as TextAsset;
        if (_Text != null)
        {
            jsonString = _Text.text;
        }
        else
        {
            jsonString = JsonUtility.ToJson(new LocalizeData(language));
        }
#endif
        LoadJson(jsonString);
    }

    /// <summary>
    /// データをセーブする
    /// </summary>
    public void Save()
    {
        File.WriteAllText(string.Format("Assets/Resources/{0}.json",GetSaveDataPath(localizeLanguage)), this.SaveToJson());
    }

    /// <summary>
    /// データ保存階層
    /// </summary>
    /// <returns></returns>
    private static string GetSaveDataPath(SystemLanguage language)
    {
        string path = string.Format("{0}/LocalizeText_{1}", filePath, language.ToString());
        return path;
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(LocalizeData))]
    public class LocalizeDataEditor : Editor
    {
        string filename;

        public override void OnInspectorGUI()
        {
            LocalizeData data = target as LocalizeData;

            EditorGUILayout.LabelField("ファイル名", string.Format("LocalizeData_{0}", data.localizeLanguage.ToString()));
            data.localizeLanguage = (SystemLanguage)EditorGUILayout.EnumPopup("ローカライズコード", data.localizeLanguage);

            serializedObject.Update();

            // 追加したらここに追加分を入れる
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_Title"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_System"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_StageSelect"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_Menu"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_Ingame"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Category_Data"), true);

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("保存"))
            {
                data.Save();
            }
        }
    }
#endif // UNITY_EDITOR
}
