﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.IO;
using YFW;

/// <summary>
/// ステージデータ (ステージごとに用意)
/// </summary>
[Serializable]
public class StageData : JsonData
{
    /// <summary>
    /// 多角形の構成データ
    /// </summary>
    [Serializable]
	public class shapeData{
		public int groupid;
		public AppDefine.eColorId[] trianglesColorId;
    }
    /// <summary>
    /// 交換の対となる情報データ
    /// </summary>
    [Serializable]
    public class ChangeData
    {
        public int _groupA;
        public int _indexA;
        public int _groupB;
        public int _indexB;
        public ChangeData(int grpA, int idxA, int grpB, int idxB) { _groupA = grpA; _indexA = idxA; _groupB = grpB; _indexB = idxB; }
    }

    public int    shapetype;
	public int    shapenum;

    /// <summary>
    /// The shapes.
    /// </summary>
    [SerializeField]
    public shapeData[] shapes;
    /// <summary>
    /// The Pairs.
    /// </summary>
    [SerializeField]
    public ChangeData[] pairs;
    
	readonly static string filePath = "StageData";

	/// <summary>
	/// 保持するJsonString
	/// </summary>
	[SerializeField]
	private string jsonString = "";

	public void ReLoad()
	{
		//LoadJson(GetJson());
	}

	/// <summary>
	/// データロード
	/// </summary>
	public void Load(int _shapetype, int _shapenum)
	{
        LoadJson(GetJson(_shapetype, _shapenum));
	}

	/// <summary>
	/// 保存しているJsonを取得する
	/// </summary>
	/// <returns></returns>
	private string GetJson(int _shapetype, int _shapenum)
	{
		//Jsonを保存している場所のパスを取得。
		string filePath = GetDataPath(_shapetype,_shapenum);

#if false
        //Jsonが存在するか調べてから取得し変換する。存在しなければ新たなクラスを作成し、それをJsonに変換する。
        if (File.Exists(filePath))
		{
			jsonString = File.ReadAllText(filePath);
		}
		else
		{
			jsonString = JsonUtility.ToJson(new StageData());
		}
#else
        TextAsset _Text = Resources.Load(filePath) as TextAsset;
        if (_Text != null)
        {
            jsonString = _Text.text;
        }
        else
        {
            jsonString = JsonUtility.ToJson(new StageData());
        }
#endif
        return jsonString;
	}

	/// <summary>
	/// データをセーブする
	/// </summary>
	public void Save()
	{
        File.WriteAllText(string.Format("Assets/Resources/{0}.json", GetDataPath(shapetype, shapenum)), this.SaveToJson());
    }

	/// <summary>
	/// データ削除
	/// </summary>
	public void Delete()
	{
		jsonString = JsonUtility.ToJson(new StageData());
		ReLoad();
	}

	/// <summary>
	/// セーブデータ階層
	/// </summary>
	/// <returns></returns>
	private static string GetDataPath(int _shapetype, int _shapenum)
	{
		string path = string.Format("{0}/StageData_{1:00}_{2:0}", filePath, _shapetype,_shapenum);
		return path;
    }
}
