﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 固定のゲームリソース スクリプタブルクラス
/// </summary>
[CreateAssetMenu]
public class GameResourceData : ScriptableObject {

    [SerializeField]
    protected int mFood;           //!< 食料
    [SerializeField]
    protected int mWood;           //!< 木材
    [SerializeField]
    protected int mStone;          //!< 石材
    [SerializeField]
    protected int mIron;           //!< 鉄材
    [SerializeField]
    protected int mMoney;          //!< 財貨
    [SerializeField]
    protected int mSword;          //!< 剣
    [SerializeField]
    protected int mBow;            //!< 弓
    [SerializeField]
    protected int mPower;          //!< 戦闘力
    [SerializeField]
    protected int mVictoryPoint;   //!< 勝利点

    /// <summary>
    /// 食料
    /// </summary>
    public int Food { get { return mFood; } }
    /// <summary>
    /// 木材
    /// </summary>
    public int Wood { get { return mWood; } }
    /// <summary>
    /// 石材
    /// </summary>
    public int Stone { get { return mStone; } }
    /// <summary>
    /// 鉄材
    /// </summary>
    public int Iron { get { return mIron; } }
    /// <summary>
    /// 財貨
    /// </summary>
    public int Money { get { return mMoney; } }
    /// <summary>
    /// 剣
    /// </summary>
    public int Sword { get { return mSword; } }
    /// <summary>
    /// 弓
    /// </summary>
    public int Bow { get { return mBow; } }
    /// <summary>
    /// 戦闘力
    /// </summary>
    public int Power { get { return mPower; } } 
    /// <summary>
    /// 勝利点
    /// </summary>
    public int VictoryPoint { get { return mVictoryPoint; } }

}

/// <summary>
/// ゲーム内計算用可変のリソース
/// </summary>
public class GameResource : GameResourceData
{
    /// <summary>
    /// 食料
    /// </summary>
    new public int Food { get { return mFood; } set { mFood = value; } }
    /// <summary>
    /// 木材
    /// </summary>
    new public int Wood { get { return mWood; } set { mWood = value; } }
    /// <summary>
    /// 石材
    /// </summary>
    new public int Stone { get { return mStone; } set { mStone = value; } }
    /// <summary>
    /// 鉄材
    /// </summary>
    new public int Iron { get { return mIron; } set { mIron = value; } }
    /// <summary>
    /// 財貨
    /// </summary>
    new public int Money { get { return mMoney; } set { mMoney = value; } }
    /// <summary>
    /// 剣
    /// </summary>
    new public int Sword { get { return mSword; } set { mSword = value; } }
    /// <summary>
    /// 弓
    /// </summary>
    new public int Bow { get { return mBow; } set { mBow = value; } }
    /// <summary>
    /// 戦闘力
    /// </summary>
    new public int Power { get { return mPower; } set { mPower = value; } }
    /// <summary>
    /// 勝利点
    /// </summary>
    new public int VictoryPoint { get { return mVictoryPoint; } set { mVictoryPoint = value; } }

    
    /// <summary>
    /// 十分なリソースが指定リソース以上存在しているかチェック
    /// </summary>
    /// <param name="resource"></param>
    /// <returns></returns>
    public bool EnoughGameResourceData(GameResource resource)
    {
        if ( Food         < resource.Food         ) { return false; }
        if ( Wood         < resource.Wood         ) { return false; }
        if ( Stone        < resource.Stone        ) { return false; }
        if ( Iron         < resource.Iron         ) { return false; }
        if ( Money        < resource.Money        ) { return false; }
        if ( Sword        < resource.Sword        ) { return false; }
        if ( Bow          < resource.Bow          ) { return false; }
        if ( Power        < resource.Power        ) { return false; }
        if ( VictoryPoint < resource.VictoryPoint ) { return false; }
        return true;
    }

    /// <summary>
    /// 十分なリソースが指定リソース以上存在しているかチェック
    /// </summary>
    /// <param name="resource"></param>
    /// <returns></returns>
    public bool EnoughGameResourceData(GameResourceData resource)
    {
        return EnoughGameResourceData(resource as GameResource);
    }


    /// <summary>
    /// リソースを加算する
    /// </summary>
    /// <param name="resource"></param>
    public void AddGameResourceData(GameResource resource)
    {
        Food         += resource.Food;
        Wood         += resource.Wood;
        Stone        += resource.Stone;
        Iron         += resource.Iron;
        Money        += resource.Money;
        Sword        += resource.Sword;
        Bow          += resource.Bow;
        Power        += resource.Power;
        VictoryPoint += resource.VictoryPoint;
    }

    /// <summary>
    /// リソースを加算する
    /// </summary>
    /// <param name="resource"></param>
    public void AddGameResourceData(GameResourceData resource)
    {
        AddGameResourceData(resource as GameResource);
    }

    /// <summary>
    /// リソースを減算する
    /// </summary>
    /// <param name="resource"></param>
    public void SubGameResourceData(GameResource resource)
    {
        Food         -= resource.Food;
        Wood         -= resource.Wood;
        Stone        -= resource.Stone;
        Iron         -= resource.Iron;
        Money        -= resource.Money;
        Sword        -= resource.Sword;
        Bow          -= resource.Bow;
        Power        -= resource.Power;
        VictoryPoint -= resource.VictoryPoint;
    }

    /// <summary>
    /// リソースを減算する
    /// </summary>
    /// <param name="resource"></param>
    public void SubGameResourceData(GameResourceData resource)
    {
        SubGameResourceData(resource as GameResource);
    }
    
    /// <summary>
    /// リソースを代入する（バックアップなどに利用する）
    /// </summary>
    /// <param name="resource"></param>
    public void AssignResourceData(GameResource resource)
    {
        Food         = resource.Food;
        Wood         = resource.Wood;
        Stone        = resource.Stone;
        Iron         = resource.Iron;
        Money        = resource.Money;
        Sword        = resource.Sword;
        Bow          = resource.Bow;
        Power        = resource.Power;
        VictoryPoint = resource.VictoryPoint;
    }

    /// <summary>
    /// リソースを代入する
    /// </summary>
    /// <param name="resource"></param>
    public void AssignGameResourceData(GameResourceData resource)
    {
        AssignResourceData(resource as GameResource);
    }

    /// <summary>
    /// リソースを積算する
    /// </summary>
    /// <param name="mul"></param>
    public void MulGameResourceData( int mul )
    {
        Food         *= resource.Food;
        Wood         *= resource.Wood;
        Stone        *= resource.Stone;
        Iron         *= resource.Iron;
        Money        *= resource.Money;
        Sword        *= resource.Sword;
        Bow          *= resource.Bow;
        Power        *= resource.Power;
        VictoryPoint *= resource.VictoryPoint;
    }
        
    /// <summary>
    /// リソースのクリア
    /// </summary>
    public void ClearResource()
    {
        Food         = 0;
        Wood         = 0;
        Stone        = 0;
        Iron         = 0;
        Money        = 0;
        Sword        = 0;
        Bow          = 0;
        Power        = 0;
        VictoryPoint = 0;
    }
}