﻿
#define FAST_BLACK

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using YFW;

/// <memo>アプリケーション通して生きているクラス</memo>
public class AppMain : YFW.SystemManager<AppMain>
{

    // ロゴアニメーション
    Animator mAnimator;

    /// <summary>
    /// システムステップ
    /// </summary>
    public enum eSystemStep
    {
        NONE = -1,
        LOGO,
        TITLESCENE_LOAD,
        TITLE,
        TITLE_END,
        MENU,
        MENU_END,
        INGAMESCENE_LOAD,
        INGAME,
        BACK_TO_STAGESELECT,
        BACK_TO_TITLE,
    };
    public eSystemStep mSystemStep = eSystemStep.NONE;

    /// <summary>
    /// シーンタイプ
    /// </summary>
    public enum eSceneType
    {
        NONE = -1,
        TITLE,
        MENU,
        INGAME,
    }
    private eSceneType mSceneType = eSceneType.NONE;    // 現在のシーン
    // シーン対応表
    private Dictionary<eSceneType, string> mSceneNameDict = new Dictionary<eSceneType, string>()
    {
        { eSceneType.TITLE, "Scene/Title" },
        { eSceneType.MENU, "Scene/Menu" },
        { eSceneType.INGAME, "Scene/ingame" },
    };

    public readonly int waitBlackHash = Animator.StringToHash("BlackIdle");
    public readonly int waitIdleHash = Animator.StringToHash("Idle");

    public bool mbSceneChangeWait = false;  // シーン遷移中
    public bool mbFadeWait = false;         // フェード遷移中


    public struct StageInfo_T
    {
        //public AppDefine.eShapeNum _shapeNum;
        public int                 _colorNum;
        public bool                _randomize;

        //public StageInfo_T(AppDefine.eShapeNum shapeNum, int colorNum, bool random) { _shapeNum = shapeNum; _colorNum = colorNum; _randomize = random; }
    };
    public StageInfo_T stageInfo;
    //public GenRandomColorShape mRandomColorShape;

    /// <summary>
    /// 汎用OKサウンド
    /// </summary>
    public static void SE_OK() { if (AudioManager.instance) AudioManager.instance.PlaySe("se_ok"); }
    /// <summary>
    /// 汎用CANCELサウンド
    /// </summary>
    public static void SE_CANCEL() { if (AudioManager.instance) AudioManager.instance.PlaySe("se_cancel"); }
    /// <summary>
    /// 汎用セレクトサウンド
    /// </summary>
    public static void SE_SELECT() { if (AudioManager.instance) AudioManager.instance.PlaySe("se_sel"); }


    static bool _pause = false;
    public static bool isPause { get { return _pause; } }
    /// <summary>
    /// アプリケーションポーズ制御
    /// </summary>
    public static void Pause(bool pause )
    {
        if ((Time.timeScale != 0f) && (pause == true))
        {
            Time.timeScale = 0f;
            _pause = true;
        }
        else if (pause == false)
        {
            Time.timeScale = 1.0f;
            _pause = false;
        }
    }

    // シーン管理.
    private void Start()
    {
        mAnimator = GetComponent<Animator>();
        if (mAnimator)
        {
#if FAST_BLACK
            mAnimator.SetTrigger("FastBlack");
            requestChangeScene(eSceneType.TITLE);
            mSystemStep = eSystemStep.TITLESCENE_LOAD;
#else
            mAnimator.SetTrigger("GameStart");
            mSystemStep = eSystemStep.LOGO;
#endif
        }
    }

    private void Update()
    {
        UpdateStep();
    }

    /// <summary>
    /// アプリケーションステップ実行
    /// </summary>
    private void UpdateStep()
    {
        switch (mSystemStep)
        {
            case eSystemStep.LOGO:
                // アニメーションチェック
                var animState = mAnimator.GetCurrentAnimatorStateInfo(0);
                if (animState.shortNameHash == waitBlackHash)
                {   // Tltleシーンロード要求
                    requestChangeScene(eSceneType.TITLE);
                    mSystemStep = eSystemStep.TITLESCENE_LOAD;
                }
                break;
            case eSystemStep.TITLESCENE_LOAD:
                if (!mbSceneChangeWait)
                {   // タイトル読み込み完了した.
                    requestBlackOut();
                    mSystemStep = eSystemStep.TITLE;
                    //AudioManager.instance.PlayBGM("bgm");
                    AudioManager.instance.PlayBGM("Mastering_long");
                }
                break;
            case eSystemStep.TITLE:
                // アクションなし、アイドル監視
                break;
            case eSystemStep.TITLE_END:
            case eSystemStep.BACK_TO_STAGESELECT:
                if (!mbFadeWait)
                {
                    // ウェイト完了した
                    requestChangeScene(eSceneType.MENU);
                    mSystemStep = eSystemStep.MENU;
                }
                break;
            case eSystemStep.MENU:
                // アクションなし、アイドル監視
                break;
            case eSystemStep.MENU_END:
                if (!mbFadeWait)
                {
                    // ウェイト完了した
                    requestChangeScene(eSceneType.INGAME);
                    mSystemStep = eSystemStep.INGAMESCENE_LOAD;
                }
                break;
            case eSystemStep.INGAMESCENE_LOAD:
                if (!mbSceneChangeWait)
                {   // Ingame読み込み完了した.
                    mSystemStep = eSystemStep.INGAME;
                }
                break;
            case eSystemStep.INGAME:
                // アクションなし、アイドル監視
                break;
            case eSystemStep.BACK_TO_TITLE:
                if (!mbFadeWait)
                {   // フェード完了したらシーン読み込み
                    requestChangeScene(eSceneType.TITLE);
                    mSystemStep = eSystemStep.TITLESCENE_LOAD;
                }
                break;
        }
    }

    private void UpdateBackKeyInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_ANDROID
            LocalizeManager LocMng = LocalizeManager.instance;
            DialogManager.Instance.SetLabel(LocMng.getContents("Category_System", "Cancel"), LocMng.getContents("Category_System", "Exit"), LocMng.getContents("Category_Menu", "Option_OK"));
            DialogManager.Instance.ShowSelectDialog(
                LocMng.getContents("Category_System", "QuitApp"),
                (bool ret) =>
                {
                    if( ret == false)
                    {
                        Application.Quit();
                    }
                }
                );
#else
            Debug.Log("Push BackKey!!");
#endif
        }
    }

    /// <summary>
    /// ステージセレクトへ戻る
    /// </summary>
    public void BackToStageSelect()
    {
        requestBlackIn();
        mSystemStep = eSystemStep.BACK_TO_STAGESELECT;
    }

    /// <summary>
    /// タイトルへ戻る
    /// </summary>
    public void BackToTitle()
    {
        requestBlackIn();
        mSystemStep = eSystemStep.BACK_TO_TITLE;
    }

    /// <summary>
    /// フェードイン要求
    /// </summary>
    public void requestBlackIn()
    {
        if (mbFadeWait) return;
        if (mAnimator)
        {
            mAnimator.ResetTrigger("BlackOut");
            mAnimator.SetTrigger("BlackIn");
            StartCoroutine(_checkFadeMode(true));
            mbFadeWait = true;
        }
    }

    /// <summary>
    /// フェードアウト要求
    /// </summary>
    public void requestBlackOut()
    {
        if (mbFadeWait) return;
        if (mAnimator)
        {
            mAnimator.ResetTrigger("BlackIn");
            mAnimator.SetTrigger("BlackOut");
            StartCoroutine(_checkFadeMode(false));
            mbFadeWait = true;
        }
    }

    /// <summary>
    /// フェード遷移待機
    /// </summary>
    /// <param name="blackin"></param>
    /// <returns></returns>
    private IEnumerator _checkFadeMode(bool blackin)
    {
        int hashkey = (blackin) ? waitBlackHash : waitIdleHash;
        var wait = new WaitForEndOfFrame();
        while(true)
        {
            var animState = mAnimator.GetCurrentAnimatorStateInfo(0);
            if (animState.shortNameHash == hashkey)
            {
                break;
            }
            yield return wait;
        }
        mbFadeWait = false;
    }

    /// <summary>
    /// シーン遷移要求
    /// </summary>
    /// <param name="nextScene">遷移先のシーン</param>
    public void requestChangeScene(eSceneType nextScene)
    {
        mbSceneChangeWait = true;
        StartCoroutine(_changeScene(nextScene));
    }

    /// <summary>
    /// シーン遷移待機
    /// </summary>
    /// <param name="nextScene">遷移先のシーン</param>
    /// <returns></returns>
    private IEnumerator _changeScene(eSceneType nextScene)
    {
        AsyncOperation operation;
        var wait = new WaitForEndOfFrame();
        // 元のシーンがあればアンロードを先に行う。
        if (mSceneType != eSceneType.NONE)
        {
            operation = SceneManager.UnloadSceneAsync(mSceneNameDict[mSceneType]);
            while (!operation.isDone)
            {
                yield return wait;
            }
        }

        // 次のシーンのロードを行う。
        operation = SceneManager.LoadSceneAsync(mSceneNameDict[nextScene], LoadSceneMode.Additive);
        while (!operation.isDone)
        {
            yield return wait;
        }
        mSceneType = nextScene;
        mbSceneChangeWait = false;
    }


}
