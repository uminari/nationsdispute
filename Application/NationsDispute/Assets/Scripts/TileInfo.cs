﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileInfo : MonoBehaviour {

    /// <summary>
    /// 領土化されているか
    /// </summary>
    public bool mbTerritorialization;
    /// <summary>
    /// 帰属先プレイヤー
    /// </summary>
    public Player mAttrPlayer;
    /// <summary>
    /// タイルの位置
    /// </summary>
    Vector3Int m_TilePosition;

    /// <summary>
    /// 自然タイプ
    /// </summary>
    public AppDefine.eNatureTileType mNature;
    /// <summary>
    /// 建造物タイプ
    /// </summary>
    public AppDefine.eBuildingTileType mBuilding;
    /// <summary>
    /// 建造物レベル
    /// </summary>
    public int mBuildingLevel;

    /// <summary>
    /// リソースの更新がかかる修正が発生した場合trueになる
    /// </summary>
    private bool mbResourceUpdate;
    /// <summary>
    /// 自然タイルリソース出力
    /// </summary>
    private GameResourceData mNatureResource;
    /// <summary>
    /// 建造物タイルリソース出力
    /// </summary>
    private GameResourceData mBuildingResource;
    /// <summary>
    /// 建物を建てているかどうか
    /// </summary>
    bool mbBuilding;

    /// <summary>
    /// タイル出力計算用ゲームリソース
    /// </summary>
    private GameResource mTotalGameResource;

    /// <summary>
    /// 現在タイルに乗っている国民ユニット
    /// </summary>
    private Citizen mOnCitizen;

    /// <summary>
    /// 現在タイルに乗ってる戦闘ユニット
    /// </summary>
    private Citizen mOnSolder;

    /// <summary>
    /// 建造物とタイルに乗っているユニットによる防御力ボーナス
    /// </summary>
    public int Defence {  get { return (mOnCitizen != null) ? mBuildingResource.Power + mOnCitizen.Power : mBuildingResource.Power; } }


    /// <summary>
    /// タイル情報の初期化
    /// </summary>
    public void initializeTileInfo()
    {
        mbTerritorialization = false;
        mAttrPlayer = null;
        mNature = AppDefine.eNatureTileType.INVALID;
        mBuilding = AppDefine.eBuildingTileType.Invalid;
        mbResourceUpdate = false;
        mbBuilding = false;
        mTotalGameResource = new GameResource();
        mOnCitizen = null;
    }
    
    /// <summary>
    /// タイルのリソース出力を計算して返却する
    /// </summary>
    /// <returns></returns>
    public GameResource GetGameResource()
    {
        if (mbResourceUpdate == false) return mTotalGameResource;

        mTotalGameResource.ClearResource();
        mTotalGameResource.AddGameResourceData(mNatureResource);
        // 建物が存在する場合のみ建物リソース追加
        if (mbBuilding)
        {
            if (mBuilding == AppDefine.eBuildingTileType.ArmsShop)
            {
                //if( (-mBuildingResource.Iron) > play )
                mTotalGameResource.AddGameResourceData(mBuildingResource);
            }
            else if (mBuilding == AppDefine.eBuildingTileType.WorkShop)
            {
                mTotalGameResource.AddGameResourceData(mBuildingResource);
            }
            else
            {
                mTotalGameResource.AddGameResourceData(mBuildingResource);
            }
        }
        if (mOnCitizen != null)
        {
            // 労働者の場合現在のリソースを倍にする
            if (mOnCitizen.mType == AppDefine.eCitizenType.Worker)
            {
                mTotalGameResource.MulGameResourceData(2);
            }
            else
            {
                mTotalGameResource.AddGameResourceData(mOnCitizen.mOutputResource);
            }
        }
        mbResourceUpdate = false;
        return mTotalGameResource;
    }

    /// <summary>
    /// 境界の設定(開拓民が新しく領土に組み入れた場合に呼び出す)
    /// </summary>
    /// <param name="newPlayer"></param>
    /// <returns></returns>
    public bool DemarcationOfLand(Player newPlayer)
    {
        mAttrPlayer = newPlayer;
        mbTerritorialization = true;
        return true;
    }

    /// <summary>
    /// 帰属先の変更(戦争によって既に領有済の領土を切り取った場合に呼び出す)
    /// </summary>
    /// <param name="newPlayer"></param>
    /// <returns></returns>
    public bool changeAttibution(Player newPlayer)
    {
        // 
        mAttrPlayer = newPlayer;
        return true;
    }

    /// <summary>
    /// 新しい建造物を建てる
    /// </summary>
    /// <param name="type">建設する建造物</param>
    /// <param name="level">建造レベル（最初から改修済の場合に1以外を設定する)</param>
    public bool setBuilding(AppDefine.eBuildingTileType type, int level )
    {
        // 不正チェック
        if( type == AppDefine.eBuildingTileType.Invalid) { Debug.LogErrorFormat("typeが不正です! {0}",type); return false; }
        if (checkBuildingLevel(type, level) == false) { Debug.LogErrorFormat("levelが不正です! {0}", level); return false; }
        mbBuilding = true;
        mBuilding = type;
        mBuildingLevel = level;
        // TODO : リソースデータの設定
        //mBuildingResource = 
        
        // TODO : 建造 エフェクトの追加
        mbResourceUpdate = true;
        return true;
    }

    /// <summary>
    /// 建造物を改修する
    /// </summary>
    public bool updateBuilding( int level )
    {
        if( checkBuildingLevel(mBuilding, level) == false) { Debug.LogErrorFormat("levelが不正です! {0}", level); return false; }
        mBuildingLevel = level;
        // TODO : リソースデータの設定
        //mBuildingResource = 

        // TODO : 改修 エフェクトの追加
        return true;
    }

    /// <summary>
    /// 建造物レベルが適切な範囲かチェックする
    /// </summary>
    /// <param name="type">建造物タイプ</param>
    /// <param name="level">チェックするレベル</param>
    /// <returns></returns>
    public bool checkBuildingLevel(AppDefine.eBuildingTileType type, int level)
    {
        switch (type)
        {
            case AppDefine.eBuildingTileType.Farm:
            case AppDefine.eBuildingTileType.Market:
            case AppDefine.eBuildingTileType.GatherersHuts:
            case AppDefine.eBuildingTileType.WoodCutter:
            case AppDefine.eBuildingTileType.Fishboat:
            case AppDefine.eBuildingTileType.Harbor:
                // Levelが2まで存在する
                if ( level < 1) { return false; }
                if( level > 2) { return false; }
                break;
            default:
                if( level != 1) { return false; }
                break;
        }
        return true;
    }




    // それぞれのレイヤーにタイルが適切に描画されているかチェックする。



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
